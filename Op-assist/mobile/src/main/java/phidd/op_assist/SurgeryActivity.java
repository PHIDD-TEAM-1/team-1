package phidd.op_assist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.R.drawable;
import java.util.Calendar;

public class SurgeryActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private int activeSurgeryYear;
    private int activeSurgeryMonth;
    private int activeSurgeryDay;
    private String activeSurgeryName;
    private int activeSurgeryID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surgery_layout);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        activeSurgeryYear = year;
        activeSurgeryDay = dayOfMonth;
        activeSurgeryMonth = monthOfYear + 1;

        String txt = "Your " + activeSurgeryName + " surgery has been scheduled for " +
                activeSurgeryMonth + "/" + activeSurgeryDay + "/" + activeSurgeryYear + ".";
        sendNotification(txt);

        //Write to shared preferences
        SharedPreferences prefs = getPrefs(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.prefs_active_surgery_name),activeSurgeryName);
        editor.putInt(getString(R.string.prefs_active_surgery_id), activeSurgeryID);
        editor.putInt(getString(R.string.prefs_surg_day),activeSurgeryDay);
        editor.putInt(getString(R.string.prefs_surg_mon),activeSurgeryMonth);
        editor.putInt(getString(R.string.prefs_surg_yr),activeSurgeryYear);
        editor.apply();

        //Return to start screen
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void setupSurgery(View v) {
        //Determine surgery by comparing IDs
        if (v.getId() == R.id.kneeButton) {
            activeSurgeryName = "knee";
            activeSurgeryID = 1;
        } else {
            activeSurgeryName = "ACL";
            activeSurgeryID = 2;
        }

        showDatePickerDialog();
    }

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    private void sendNotification(String notifyText) {
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle("Op-Assist")
                .setSmallIcon(drawable.stat_notify_chat)
                .setContentText(notifyText)
                .setVibrate(new long[]{0,500})
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        notificationManager.notify(001, notificationBuilder.build());
    }

    private void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), (SurgeryActivity)getActivity(), year, month, day);
        }
    }
}



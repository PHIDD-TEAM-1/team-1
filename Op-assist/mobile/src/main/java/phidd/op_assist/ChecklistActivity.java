package phidd.op_assist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ChecklistActivity extends AppCompatActivity {
    TaskAdapter dataAdapter = null;
    private static String TAG = "CHECKLIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_menu);
    }

    public void showChecklist(View v) {
        String list;
        if (v.getId() == R.id.preopButton) {
            list = "preop";
        } else if (v.getId() == R.id.postopButton) {
            list = "postop";
        } else if (v.getId() == R.id.daybefsurgButton) {
            list = "daybefsurg";
        } else {
            list = "prephome";
        }
        displayListView(list);
    }

    private void displayListView(String list) {
        setContentView(R.layout.activity_checklist_list);
        TextView checklistHeaderText = (TextView)findViewById(R.id.checklistHeaderText);
        checklistHeaderText.setText(getStringResourceByName(list));

        //Load tasks
        int i = 1;
        boolean tasksExist = true;
        ArrayList<Task> taskList = new ArrayList<>();
        while (tasksExist) {
            if (getResources().getIdentifier(list + "_" + i,"string",getPackageName()) != 0) {
                //Add task to ArrayList
                Task task = new Task(getStringResourceByName(list + "_" + i), false);
                taskList.add(task);
                i++;
            } else {
                tasksExist = false;
            }
        }

        //create an ArrayAdapter from the String Array
        dataAdapter = new TaskAdapter(this,
                R.layout.checkbox_timeline, taskList, TAG + list);
        ListView listView = (ListView) findViewById(R.id.checklist_listview);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Task task = (Task) parent.getItemAtPosition(position);
            }
        });
    }

    private String getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        return getString(resId);
    }
}

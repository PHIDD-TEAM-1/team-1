package phidd.op_assist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_surgeryinfo);
        TextView infoView = (TextView)findViewById(R.id.surgInfoText);
        infoView.setMovementMethod(new ScrollingMovementMethod());
    }
}

package phidd.op_assist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    int currentSurgery;
    int surgDay;
    int surgMonth;
    int surgYear;
    int milestone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Determine whether there is a scheduled surgery
        SharedPreferences prefs = getPrefs(this);
        currentSurgery = prefs.getInt(getString(R.string.prefs_active_surgery_id), 0);

        if (currentSurgery > 0) {
            surgDay = prefs.getInt(getString(R.string.prefs_surg_day),1);
            surgMonth = prefs.getInt(getString(R.string.prefs_surg_mon),1);
            surgYear = prefs.getInt(getString(R.string.prefs_surg_yr),9999);
            String activeSurgeryName = prefs.getString(getString(R.string.prefs_active_surgery_name),"null");
            int milestoneFromPrefs = prefs.getInt(getString(R.string.prefs_milestone),909);

            //Open new layout for a scheduled surgery
            setContentView(R.layout.activity_main_dashboard);

            //Calculate days until/since surgery
            Calendar today = Calendar.getInstance();

            Calendar surgeryDay = Calendar.getInstance();
            surgeryDay.set(Calendar.DAY_OF_MONTH, surgDay);
            surgeryDay.set(Calendar.MONTH, surgMonth);
            surgeryDay.set(Calendar.YEAR, surgYear);

            //long daysTilSurg = 29 - getDateDiff(today.getTime(), surgeryDay.getTime(), TimeUnit.DAYS);
            long daysTilSurg = daysBetween(today, surgeryDay) - 31;

            //Display info
            TextView surgeryInfoView = (TextView)findViewById(R.id.surgeryInfo);
            boolean past;
            if (today.getTime().before(surgeryDay.getTime())) { //surgery is coming up
                past = false;
                if (daysTilSurg == 1) {
                    surgeryInfoView.setText(String.format("Your %s surgery is scheduled for %d/%d/%d. That is " + Long.toString(Math.abs(daysTilSurg)) + " day from now.",
                            activeSurgeryName, surgMonth, surgDay, surgYear));
                } else {
                    surgeryInfoView.setText(String.format("Your %s surgery is scheduled for %d/%d/%d. That is " + Long.toString(Math.abs(daysTilSurg)) + " days from now.",
                            activeSurgeryName, surgMonth, surgDay, surgYear));
                }
                daysTilSurg = -daysTilSurg;
            } else { //surgery was in the past
                past = true;
                if (daysTilSurg == 1) {
                    surgeryInfoView.setText(String.format("Your %s surgery was scheduled for %d/%d/%d. That was " + Long.toString(Math.abs(daysTilSurg)) + " day ago.",
                            activeSurgeryName, surgMonth, surgDay, surgYear));
                } else {
                    surgeryInfoView.setText(String.format("Your %s surgery was scheduled for %d/%d/%d. That was " + Long.toString(Math.abs(daysTilSurg)) + " days ago.",
                            activeSurgeryName, surgMonth, surgDay, surgYear));
                }
            }

            //Find closest milestone
            if (currentSurgery == 1) { //Knee surgery

                //milestones for knee surgery
                //negative = days til, positive = days after, 0 = day of
                List<Integer> milestoneList = Arrays.asList(-30,-14,-11,-8,-7,-5,-4,-3,-2,-1,0,
                        1,2,3,4,5,7,10,14,21,28,60);

                int[] closest_out = closest((int)daysTilSurg, milestoneList);
                int closestIdx = closest_out[0];
                int daysTilMilestone = closest_out[1];

                int closest = milestoneList.get(closestIdx);
                int milestoneIdx;
                if (daysTilSurg > closest && closestIdx < milestoneList.size()-1) {
                    //Increment index so that the next coming milestone is displayed, unless
                    //we are on the last milestone
                    milestone = milestoneList.get(closestIdx+1);
                    milestoneIdx = closestIdx+1;
                } else {
                    milestone = milestoneList.get(closestIdx);
                    milestoneIdx = closestIdx;
                }

                //Write to shared preferences
                SharedPreferences.Editor editor = prefs.edit();
                boolean milestoneChanged = milestoneFromPrefs != milestone;
                editor.putInt(getString(R.string.prefs_milestone), milestone);
                editor.putInt(getString(R.string.prefs_milestone_idx),milestoneIdx);
                editor.putInt(getString(R.string.prefs_milestone_daysTil),daysTilMilestone);
                editor.putInt(getString(R.string.prefs_surgery_daysTil),(int)Math.abs(daysTilSurg));
                editor.putBoolean(getString(R.string.prefs_surg_past), past);
                editor.putBoolean(getString(R.string.prefs_milestone_changed),milestoneChanged);
                editor.apply();
            }
        } else {
            setContentView(R.layout.activity_main);

            ImageView logoView = (ImageView)findViewById(R.id.logoView);
            logoView.setImageResource(R.drawable.logo);
        }
    }

    // Button functions
    public void startSurgeryActivity(View v) {
        Intent intent = new Intent(this, SurgeryActivity.class);
        startActivity(intent);
    }

    public void openSurgInfo(View v) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

    public void openTimeline(View v) {
        Intent intent = new Intent(this, TimelineActivity.class);
        startActivity(intent);
    }

    public void openChecklists(View v) {
        Intent intent = new Intent(this, ChecklistActivity.class);
        startActivity(intent);
    }

    public void openFamSupport(View v) {
        Intent intent = new Intent(this, FamilySupportActivity.class);
        startActivity(intent);
    }

    public void openAppts(View v) {
        Intent intent = new Intent(this, ApptsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.clear_surgery) {
            SharedPreferences prefs = getPrefs(this);
            prefs.edit().clear().commit();

            finish();
            startActivity(getIntent());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    public int[] closest(int of, List<Integer> in) {
        int min = Integer.MAX_VALUE;
        int[] closest_array = new int[2];

        int v = 0;
        while (v < in.size()) {
            final int diff = Math.abs(in.get(v) - of);

            if (diff < min) {
                min = diff;
                closest_array[0] = v;
                closest_array[1] = diff;
            }
            v++;
        }

        return closest_array;
    }

    public static int daysBetween(Calendar day1, Calendar day2){
        Calendar dayOne = (Calendar) day1.clone(),
                dayTwo = (Calendar) day2.clone();

        if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
            return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR));
        } else {
            if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
                //swap them
                Calendar temp = dayOne;
                dayOne = dayTwo;
                dayTwo = temp;
            }
            int extraDays = 0;

            int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

            while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                dayOne.add(Calendar.YEAR, -1);
                // getActualMaximum() important for leap years
                extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
            }

            return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays ;
        }
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillis = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }
}

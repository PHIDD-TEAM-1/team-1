package phidd.op_assist;

public class Task {
    String name = null;
    boolean completed = false;

    public Task(String name, boolean completed) {
        super();
        this.name = name;
        this.completed = completed;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isCompleted() {
        return completed;
    }
    public void setCompleted(boolean c) {
        this.completed = c;
    }
}

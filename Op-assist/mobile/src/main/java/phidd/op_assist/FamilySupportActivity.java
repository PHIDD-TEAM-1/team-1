package phidd.op_assist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FamilySupportActivity extends AppCompatActivity {

    private String contactName = null;
    private String contactEmail = null;
    private int currentMilestone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = getPrefs(this);

        //Get current surgical status
        currentMilestone = prefs.getInt(getString(R.string.prefs_milestone),0);

        //Test whether user has at least one contact saved
        String name = prefs.getString(getString(R.string.prefs_support_contact1_name),"null");
        if (name.equals("null")) { //no contacts, load page to add them
            setContentView(R.layout.activity_familysupport_nocontacts);
        } else {
            setContentView(R.layout.activity_familysupport_main);
            contactName = name;
            contactEmail = prefs.getString(getString(R.string.prefs_support_contact1_email),"null");

            Button ctBtn = (Button)findViewById(R.id.contact1Btn);
            ctBtn.setText(name);
        }
    }

    public void backToMain(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void showNameDialog(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please enter the name of your contact.");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactName = input.getText().toString();
                showEmailDialog();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactName = null;
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void showEmailDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please enter the email address of your contact.");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactEmail = input.getText().toString();
                saveContact(contactName, contactEmail);

                finish();
                startActivity(getIntent());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactName = null;
                contactEmail = null;
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void saveContact(String name, String email) {
        SharedPreferences prefs = getPrefs(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.prefs_support_contact1_name), name);
        editor.putString(getString(R.string.prefs_support_contact1_email), email);
        editor.commit();
    }

    public void prepEmail(View v) {

        //Get patient's current surgery information
        SharedPreferences prefs = getPrefs(this);
        boolean past = prefs.getBoolean(getString(R.string.prefs_surg_past), true);
        int surgDaysFromNow = prefs.getInt(getString(R.string.prefs_surgery_daysTil), 0);
        int surgDay = prefs.getInt(getString(R.string.prefs_surg_day), 1);
        int surgMo = prefs.getInt(getString(R.string.prefs_surg_mon),1);
        int surgYr = prefs.getInt(getString(R.string.prefs_surg_yr),1);
        String surgName = prefs.getString(getString(R.string.prefs_active_surgery_name),"null");
        //get current timeline tasks
        String currentTask = getCurrentTimelineTask();

        sendEmail(contactName, contactEmail, surgName, currentTask, surgDaysFromNow, surgDay, surgMo, surgYr, past);
    }

    public void sendEmail(String cn, String ce, String sn, String t, int daysFromNow, int d, int m, int y, boolean p) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{ce});
        i.putExtra(Intent.EXTRA_SUBJECT, cn + ", please see this update on my surgery from Op-Assist");

        if (!p) { //surgery is coming up
            i.putExtra(Intent.EXTRA_TEXT   , "Hi " + cn + ",\n\n" + "I am using Op-Assist to send you an " +
                "email update regarding my surgery. My " + sn + " surgery has been scheduled for " +
                Integer.toString(m) + "/" + Integer.toString(d) + "/" + Integer.toString(y) + ". " +
                "It is " + Integer.toString(daysFromNow) + " days away. Below is a list of all of " +
                "my current tasks. I would like your help in completing them.\n\n" + t + "\n\n" +
                "Thank you!");
        } else { //surgery was in the past
            i.putExtra(Intent.EXTRA_TEXT   , "Hi " + cn + ",\n\n" + "I am using Op-Assist to send you an " +
                "email update regarding my surgery. My " + sn + " surgery was scheduled for " +
                Integer.toString(m) + "/" + Integer.toString(d) + "/" + Integer.toString(y) + ". " +
                "It was " + Integer.toString(daysFromNow) + " days ago. Below is a list of all of " +
                "my current tasks. I would like your help in completing them.\n\n" + t + "\n\n" +
                "Thank you!");
        }

        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(FamilySupportActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    private String getCurrentTimelineTask() {
        String ms;

        //Get milestone string
        if (currentMilestone > 0) {
            ms = "Tplus" + Integer.toString(Math.abs(currentMilestone));
        } else if (currentMilestone == 0) {
            ms = "T";
        } else {
            ms = "Tminus" + Integer.toString((Math.abs(currentMilestone)));
        }
        return getStringResourceByName(ms);
    }

    private String getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        return getString(resId);
    }
}

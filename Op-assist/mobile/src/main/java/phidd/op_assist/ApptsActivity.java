package phidd.op_assist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ApptsActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    private int apptMonth;
    private int apptDay;
    private int apptYear;
    //private String apptDescription;
    private handleDate dateListener;
    private int numAppts = 0;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);

        //Remove focus from edit text
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        editText = (EditText)findViewById(R.id.appts_description);
        final Button addApptBtn = (Button)findViewById(R.id.addAptBtn);
        addApptBtn.setEnabled(false);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addApptBtn.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dateListener = new handleDate();
        displayListView();
    }

    private void displayListView() {

        SharedPreferences prefs = getPrefs(this);
        numAppts = prefs.getInt(getString(R.string.prefs_number_of_appts),0);

        final ListView listview = (ListView) findViewById(R.id.apptsList);
        if (numAppts > 0) {
            Gson gson = new Gson();
            String[] values = new String[numAppts];
            final ArrayList<String> list = new ArrayList<String>();

            for (int n = 0; n < values.length; n++) {
                int appt_index = n + 1;
                String obj = prefs.getString("APPT_" + appt_index, "0");

                Appointment a = gson.fromJson(obj, Appointment.class);
                values[n] = a.getDescription() + "\n" + a.getMonth() + "/" + a.getDay() + "/" + a.getYear() +
                        " at " + a.getHourOfDay() + ":" + a.getMinute();
                list.add(values[n]);
            }

            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    R.layout.appointments_list_item, list);

            listview.setAdapter(adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Do something

                }
            });
        }
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }

    private class handleDate implements DatePickerDialog.OnDateSetListener {
        public void onDateSet(DatePicker view, int yr, int mo, int day) {
            apptYear = yr;
            apptMonth = mo;
            apptDay = day;
            showTimePickerDialog();
        }
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,apptYear);
        c.set(Calendar.MONTH,apptMonth);
        c.set(Calendar.DAY_OF_MONTH,apptDay);
        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
        c.set(Calendar.MINUTE,minute);

        int hour = c.get(Calendar.HOUR);
        int ampm = c.get(Calendar.AM_PM);
        String ampm_str;
        if (ampm == Calendar.AM) {
            ampm_str = "AM";
        } else {
            ampm_str = "PM";
        }

        Appointment newAppt = new Appointment(Integer.toString(apptYear), Integer.toString(apptMonth),
            Integer.toString(apptDay),
            Integer.toString(hour),
            Integer.toString(minute) + " " + ampm_str,
            editText.getText().toString());

        Gson gson = new Gson();
        String json = gson.toJson(newAppt);
        numAppts++;

        SharedPreferences prefs = getPrefs(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("APPT_" + numAppts,json);
        editor.putInt(getString(R.string.prefs_number_of_appts), numAppts);
        editor.apply();

        finish();
        startActivity(getIntent());
    }

    public void addAppointment(View v) {
        showDatePickerDialog();
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(dateListener);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment {

        private DatePickerDialog.OnDateSetListener listener;

        static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatePickerFragment pickerFragment = new DatePickerFragment();
            pickerFragment.setOnDateSetListener(onDateSetListener);
            return pickerFragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), listener, year, month, day);
        }

        private void setOnDateSetListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), (ApptsActivity)getActivity(), hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }
    }

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }
}

package phidd.op_assist;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TaskAdapter extends ArrayAdapter<Task> {

    private ArrayList<Task> taskList;
    private Context mContext;
    private SharedPreferences.Editor editor;
    private String tag;

    public TaskAdapter(Context context, int textViewResourceId,
                       ArrayList<Task> taskList, String tag) {
        super(context, textViewResourceId, taskList);
        this.taskList = new ArrayList<Task>();
        this.taskList.addAll(taskList);
        this.mContext = context;
        this.tag = tag;
    }

    private class ViewHolder {
        CheckBox checkBox;
    }

    public int getCount() {
        return taskList.size();
    }

    public Task getItem(int position) {
        return taskList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Task task = taskList.get(position);
        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));
        SharedPreferences prefs = mContext.getSharedPreferences(
                mContext.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        editor = prefs.edit();

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.checkbox_timeline, null);

            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.checkBox.setText(task.getName());
        holder.checkBox.setChecked(prefs.getBoolean("CheckValue" + tag + Integer.toString(position), false));
        updateColors(holder.checkBox);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                task.setCompleted(cb.isChecked());
                editor.putBoolean("CheckValue" + tag + Integer.toString(position), cb.isChecked());
                editor.commit();
                updateColors(cb);
            }
        });

        return convertView;
    }

    public void updateColors(CheckBox cb) {
        //Change color and size of text if checked
        if (cb.isChecked()) {
            cb.setTextColor(mContext.getResources().getColor(R.color.gray));
            cb.setTextSize(15);
        } else {
            cb.setTextColor(mContext.getResources().getColor(R.color.navy));
            cb.setTextSize(20);
        }
    }
}

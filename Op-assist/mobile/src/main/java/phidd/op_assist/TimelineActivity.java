package phidd.op_assist;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TimelineActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    TaskAdapter dataAdapter = null;
    TaskAdapter nextTaskAdapter = null;
    private int currentMilestone = 0;
    private int currentSurgery = 0;
    private int currentMilestoneIdx;
    private int daysTilMilestone;
    private int daysTilNextMs;
    private int daysTilSurgery;
    private ScheduleClient scheduleClient;
    private static String TAG = "TIMELINE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        scheduleClient = new ScheduleClient(this);
        scheduleClient.doBindService();

        SharedPreferences prefs = getPrefs(this);
        currentSurgery = prefs.getInt(getString(R.string.prefs_active_surgery_id),0);
        currentMilestone = prefs.getInt(getString(R.string.prefs_milestone),0);
        currentMilestoneIdx = prefs.getInt(getString(R.string.prefs_milestone_idx),0);
        daysTilMilestone = prefs.getInt(getString(R.string.prefs_milestone_daysTil),0);
        daysTilSurgery = prefs.getInt(getString(R.string.prefs_surgery_daysTil),0);

        boolean milestoneChanged = prefs.getBoolean(getString(R.string.prefs_milestone_changed),false);
        boolean reminderSet = prefs.getBoolean(getString(R.string.prefs_timeline_reminderSet),false);

        Button b = (Button)findViewById(R.id.reminderButton);
        if (milestoneChanged) {
            b.setEnabled(true);
            b.setText(getString(R.string.reminderButtonText));
        } else {
            if (reminderSet) {
                b.setEnabled(false);
                String txt = prefs.getString(getString(R.string.prefs_timeline_reminderBtnText),getString(R.string.reminderButtonText));
                b.setText(txt);
            } else {
                b.setEnabled(true);
                b.setText(getString(R.string.reminderButtonText));
            }
        }

        displayListView();

        //Set texts
        TextView tasksNowTxt = (TextView)findViewById(R.id.tasksNow);
        String baseText = getString(R.string.tasksNow);

        if (daysTilMilestone == 0) {
            tasksNowTxt.setText(String.format("%s: today!", baseText));
        } else if (daysTilMilestone == 1){
            tasksNowTxt.setText(String.format("%s: tomorrow!", baseText));
        } else {
            tasksNowTxt.setText(String.format("%s: %d days from now", baseText,daysTilMilestone));
        }

        TextView tasksNextTxt = (TextView)findViewById(R.id.tasksNext);
        baseText = getString(R.string.tasksNext);
        if (daysTilNextMs == 1) {
            tasksNextTxt.setText(String.format("%s: tomorrow!", baseText));
        } else {
            tasksNextTxt.setText(String.format("%s: %d days from now", baseText, daysTilNextMs));
        }
    }

    @Override
    protected void onStop() {
        if (scheduleClient != null) {
            scheduleClient.doUnbindService();
        }
        super.onStop();
    }

    @Override
    public void onTimeSet (TimePicker view, int hourOfDay, int minute) {
        // Set notification alarm with chosen time
        Calendar reminderCal = Calendar.getInstance();
        reminderCal.add(Calendar.DATE, daysTilNextMs);
        reminderCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        reminderCal.set(Calendar.MINUTE, minute);

        String ampm;
        if (reminderCal.get(Calendar.AM_PM) == Calendar.AM) {
            ampm = "AM";
        } else {
            ampm = "PM";
        }

        if (scheduleClient.isBound()) {
            scheduleClient.setAlarmForNotification(reminderCal);
        }

        //Give feedback on reminder set
        Button b = (Button)findViewById(R.id.reminderButton);
        b.setEnabled(false);
        String reminderText;
        if (daysTilNextMs == 1) {
            reminderText = String.format("Reminder set for %d day from now, at %d:%d %s", daysTilNextMs, Calendar.HOUR, Calendar.MINUTE, ampm);
        } else {
            reminderText = String.format("Reminder set for %d days from now, at %d:%d %s", daysTilNextMs, Calendar.HOUR, Calendar.MINUTE, ampm);
        }
        b.setText(reminderText);

        //Save this info to prefs
        SharedPreferences prefs = getPrefs(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(getString(R.string.prefs_timeline_reminderSet), true);
        editor.putString(getString(R.string.prefs_timeline_reminderBtnText),reminderText);
        editor.apply();
    }

    private void displayListView() {

        String ms;
        //Get milestone string
        if (currentMilestone > 0) {
            ms = "Tplus" + Integer.toString(Math.abs(currentMilestone));
        } else if (currentMilestone == 0) {
            ms = "T";
        } else {
            ms = "Tminus" + Integer.toString((Math.abs(currentMilestone)));
        }

        //Load CURRENT tasks for timeline
        String taskText = getStringResourceByName(ms);

        //Add tasks to ArrayList
        ArrayList<Task> taskList = new ArrayList<>();
        Task task = new Task(taskText, false);
        taskList.add(task);

        //create an ArrayAdapter from the String Array
        dataAdapter = new TaskAdapter(this,
                R.layout.checkbox_timeline, taskList, TAG + Integer.toString(currentMilestone) + "current");
        ListView listView = (ListView) findViewById(R.id.timeline_listView_currentTasks);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Task task = (Task) parent.getItemAtPosition(position);
            }
        });

        //Load FUTURE tasks for timeline
        List<Integer> milestoneList = Arrays.asList(-30, -14, -11, -8, -7, -5, -4, -3, -2, -1, 0,
                    1, 2, 3, 4, 5, 7, 10, 14, 21, 28, 60);
        String nextTaskText;
        if (currentMilestoneIdx == milestoneList.size()-1) {
            nextTaskText = "You accomplished every goal :)";
        } else {
            int nextMilestone = milestoneList.get(currentMilestoneIdx+1);
            String nextMs;
            //Get milestone string
            if (nextMilestone > 0) {
                nextMs = "Tplus" + Integer.toString(Math.abs(nextMilestone));
            } else if (nextMilestone == 0) {
                nextMs = "T";
            } else {
                nextMs = "Tminus" + Integer.toString((Math.abs(nextMilestone)));
            }

            nextTaskText = getStringResourceByName(nextMs);
        }

        daysTilNextMs = Math.abs( milestoneList.get(currentMilestoneIdx+1) - milestoneList.get(currentMilestoneIdx)) + daysTilMilestone;

        ArrayList<Task> nextTaskList = new ArrayList<>();
        Task nextTask = new Task(nextTaskText, false);
        nextTaskList.add(nextTask);

        nextTaskAdapter = new TaskAdapter(this,
                R.layout.checkbox_timeline, nextTaskList, TAG + Integer.toString(currentMilestone) + "next");
        ListView lv_nextTasks = (ListView) findViewById(R.id.timeline_listView_nextTasks);
        // Assign adapter to ListView
        lv_nextTasks.setAdapter(nextTaskAdapter);

        lv_nextTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Task task = (Task) parent.getItemAtPosition(position);
            }
        });
    }

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    private String getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        return getString(resId);
    }

    public void setUpReminder(View v) {
        //ask if user wants to use default reminder time
        //otherwise show time picker
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), (TimelineActivity)getActivity(), hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }
    }
}

package phidd.op_assist;

public class Appointment {

    private String month;
    private String day;
    private String year;
    private String description;
    private String hourOfDay;
    private String minute;

    public Appointment(String year, String month, String day, String hour, String min, String description) {
        this.month = month;
        this.day = day;
        this.year = year;
        this.description = description;
        this.hourOfDay = hour;
        this.minute = min;
    }

    //Getter and setter methods
    public String getMonth() {return month;}
    public String getDay() {return day;}
    public String getYear() {return year;}
    public String getDescription() {return description;}
    public String getHourOfDay() {return hourOfDay;}
    public String getMinute() {return minute;}

    public void setMonth(String s) { month = s; }
    public void setDay(String s) { day = s; }
    public void setYear(String s) { year = s; }
    public void setDescription(String s) { description = s; }
    public void setHourOfDay(String s) { hourOfDay = s; }
    public void setMinute(String s) { minute = s; }
}


# README #


### General Info. ###

Op-Assist is a unique application being designed to assist patients recovering from surgery. The goal of the application is for it to function like a "surgical buddy" from start to finish- a support system that stays with you from the moment you decide you are having this operation to the time when you finish recovering. Op-Assist will function mostly as an alert system and allow you to have relevant information on your wrist. It is our hopes that this application will empower you to take care of yourself. 

Meeting Schedule: **Sundays at 5:00pm** & **Thursdays at 3:00pm** (adjusted)

Contact Info:

•Kirti: kewalramani.k@gmail.com; (516)-427-1161; skype: kirti.kewalramani1


•Xiyang: xu.xiy@husky.neu.edu; (617)-816-0558; skype: leoking081 


•Ye: pan.ye1@husky.neu.edu; (617)-850-2542; skype: selina_py


•Alex: ozzmotic@gmail.com; (510)-468-3620; skype: ozzmotic  

To join our meetings, please provide us with your skype username or cell phone number 

If you have questions- please post them to Piazza and a member of the team will get back to you. You can follow our piazza conversations under PHIDD Team 1 Group. 
Link: 
https://piazza.com/class/iec4v7cptwh1oy?cid=65


### Final presentation ###

Go to "Downloads", and find the file named as:

PHIDD-Team 1.FinalPresentation.pptx

### What is this repository for? ###

This is the repository for Op-Assist, Team 1's project in Personal Health Interface Design and Development.

If you want to download our repository, you can find it here:
https://bitbucket.org/PHIDD-TEAM-1/team-1/downloads

### Prototype for Op-Assist 

If you want to see our prototype for our app, you can find all files at Bitbucket --> Downloas:

https://bitbucket.org/PHIDD-TEAM-1/team-1/downloads

or at Dropbox:

https://www.dropbox.com/home/Interface%20Design%20Class/Previous%20prototypes

### Demo Instructions ###
https://bitbucket.org/PHIDD-TEAM-1/team-1/wiki/Demo%20Instructions

Demo Video: https://www.youtube.com/watch?v=tiohaZ8oQ8E

### Documentary Slides ###
https://prezi.com/jzbmiehpqhfr/op-assist/?utm_campaign=share&utm_medium=copy

### How do I get set up? ###

Read our meeting notes for each week. 
https://bitbucket.org/PHIDD-TEAM-1/team-1/wiki/browse/

If you want to join our meetings, please contact xu.xiy@husky.neu.edu

If you want to check out our current demo, please see this page:
https://bitbucket.org/PHIDD-TEAM-1/team-1/wiki/Demo%20Instructions

If you want to contribute to the source code, clone this repository and contact us to let us know what you're interested in developing.

### Endnote Shared Library: ###

If you want to gain access to our shared library, please install Endnote X7.4 first, then email pan.ye1@husky.neu.edu . We'd like to share it with you.

Here is some information about library sharing: http://endnote.com/product-details/library-sharing

You can download the software here: http://endnote.com/

### Next Scheduled Meeting ###

No

### Who do I talk to? ###

Technical questions: ahmed.al@husky.neu.edu

To be added to Endnote: pan.ye1@husky.neu.edu

To join meetings: xu.xiy@husky.neu.edu

All else: kewalramani.k@gmail.com